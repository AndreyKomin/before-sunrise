import mongoose from "mongoose";

const connectMongo = async () => {
  const { connection } = await mongoose.connect(process.env.MONGO_URI);

  if (connection.readyState === 1) {
    console.log("Database Connected");
  }

  return connection;
};

export default connectMongo;
