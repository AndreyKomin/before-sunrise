import { useState } from "react";
import UpdateUserForm from "./updateUserForm";
import AddUserForm from "./addUserForm";

const formReducer = (state, event) => ({
  ...state,
  [event.target.name]: event.target.value,
});

export default function Form() {
  const [formData, setFormData] = useState(); // useReducer(formReducer, {})
  const formId = ""; // () => 1; // useSelector((state) => state.app.client.formId);

  return (
    <div className="container mx-auto py-5">
      {formId
        ? UpdateUserForm({ formId, formData, setFormData })
        : AddUserForm({ formData, setFormData })}
    </div>
  );
}
